using System;
using System.Diagnostics;

namespace Exodus.Debug
{
    /// <summary>
    /// Contains the result of a profiled scope.
    /// </summary>
    public struct ProfilerResult
    {
        /// <summary>
        /// The set name of the profiled scope.
        /// </summary>
        public string Name { get; init; }
        /// <summary>
        /// The time the code took to execute in milliseconds.
        /// </summary>
        public float Time { get; init; }
    }

    /// <summary>
    /// A diagnostic tool for measuring how quickly a section of code is executed.
    /// </summary>
    public sealed class Profiler : IDisposable
    {
        private Stopwatch _clock;
        private string _name;
        
        /// <summary>
        /// Whether or not to begin profiling performance.
        /// </summary>
        public static bool Profiling { get; set; } = false;
        
        /// <summary>
        /// The results of all profiled scopes are pushed to these listeners.
        /// </summary>
        public static Action<ProfilerResult>? ResultListener;

        /// <summary>
        /// Begin a stopwatch for the profile scope and store the specified name.
        /// </summary>
        /// <param name="name">Name of scope</param>
        private Profiler(string name)
        {
            _name = name;
            _clock = Stopwatch.StartNew();
        }

        /// <summary>
        /// Profile the performance of a given scope and store the result under a set name.
        /// </summary>
        /// <example>
        /// <code>
        /// using(Profiler.Scope("MyMethod")
        /// {
        ///     // Performance-critical code
        /// }
        /// </code>
        /// </example>
        /// <param name="name">A custom-defined name for the scope of code being profiled.</param>
        /// <returns></returns>
        public static Profiler? Scope(string name)
        {
#if DEBUG
            if (Profiling) return new Profiler(name);
            return null;
#endif
            return null;
        }

        /// <summary>
        /// Profile the performance of a method and store the result under the name of the method.
        /// Using multiple of these in the same method may cause results to override each other.
        /// </summary>
        /// <example>
        /// <code>
        /// private void MyMethod()
        /// {
        ///     using(Profiler.Method())
        ///     {
        ///         // Method code
        ///     }
        /// }
        /// </code>
        /// </example>
        /// <returns></returns>
        public static Profiler? Method()
        {
#if DEBUG
            if (Profiling)
            {
                var method = new StackFrame(1).GetMethod();
                var name = method != null ? method.Name : "Unknown Method";
                return new Profiler(name);
            }

            return null;
#endif
            return null;
        }

        #region Disposable pattern

        private bool _disposed = false;

        /// <summary>
        /// Stops profiling the scope and submits the results to listeners.
        /// </summary>
        /// <param name="disposing"></param>
        private void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }

            if (disposing)
            {
                _clock.Stop();
                float elapsed = (float) _clock.ElapsedTicks / Stopwatch.Frequency * 1000;

                ResultListener?.Invoke(new ProfilerResult
                {
                    Name = _name,
                    Time = elapsed
                });
            }

            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
        }

        #endregion
    }
}