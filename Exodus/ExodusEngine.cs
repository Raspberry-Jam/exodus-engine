﻿using System;
using System.Diagnostics;
using Exodus.Graphics;
using Exodus.Input;
using NLog;
using NLog.Config;
using NLog.Targets;

namespace Exodus
{
    public sealed class ExodusEngine
    {
        #region Engine Component Instances
        /// <summary>
        /// ExodusEngine singleton instance. 
        /// </summary>
        public static ExodusEngine Instance { get; private set; }
        
        /// <summary>
        /// The window instance that the engine is running in.
        /// </summary>
        internal ExodusWindow Window { get; private set; }
        
        /// <summary>
        /// Keyboard input of the currently running engine instance.
        /// </summary>
        public KeyboardInput KeyboardInput { get; private set; }
        
        /// <summary>
        /// Handle registering and invoking key events for the current engine instance.
        /// </summary>
        public KeyEventSignaller KeyEvents { get; private set; }
        #endregion
        
        #region Engine State
        /// <summary>
        /// Milliseconds elapsed since the previous game tick.
        /// </summary>
        public long Delta { get; set; }

        /// <summary>
        /// Target tick rate for the game. Defaults to 0, which disables tick rate limiting.
        /// </summary>
        public long TargetDelta { get; set; } = 0;

        /// <summary>
        /// Determines if the main loop should exit and trigger the shutdown events.
        /// </summary>
        public bool ShouldClose { private get; set; } = false; 
        #endregion

        #region Engine Routines
        /// <summary>
        /// Handle startup tasks that need to be completed before the game can begin.
        /// </summary>
        public event Action? Startup;
        /// <summary>
        /// Run game logic.
        /// </summary>
        public event Action<long>? Tick;
        /// <summary>
        /// Handle shutdown tasks to clean up the game state after the main loop has exited.
        /// </summary>
        public event Action? Shutdown;
        #endregion

        /// <summary>
        /// Construct a new instance of the Exodus engine, and create a window with the given parameters.
        /// </summary>
        /// <param name="width">Window width in pixels</param>
        /// <param name="height">Window height in pixels</param>
        /// <param name="title">Title of window</param>
        public ExodusEngine(int width, int height, string title)
        {
            Instance = this;
            
            // Setup NLog
            // TODO: Probably make this configurable for game developer needs
            // TODO: Filter log events for ExodusEngine targets on release builds
            var layout = "[${time}] |${level:uppercase=true}| (${logger}): ${message}";
            var config = new LoggingConfiguration();
            var logFile = new FileTarget("logfile")
            {
                FileName = "exodus-logs.txt",
                Layout = layout,
                DeleteOldFileOnStartup = true
            };
            var logConsole = new ConsoleTarget("logconsole")
            {
                Layout = layout
            };

            config.AddRule(LogLevel.Info, LogLevel.Fatal, logConsole);
            config.AddRule(LogLevel.Debug, LogLevel.Fatal, logFile);

            LogManager.Configuration = config;

            Window = new ExodusWindow(width, height, title);
            KeyboardInput = new KeyboardInput();
            KeyEvents = new KeyEventSignaller();
        }

        /// <summary>
        /// The engine main loop. This is responsible for executing all the engine component routines, either on the
        /// main thread or in their own threads.
        /// </summary>
        public void Run()
        {
            Startup?.Invoke();
            var clock = new Stopwatch();
            var now = clock.ElapsedMilliseconds;

            clock.Start();
            while (!ShouldClose)
            {
                Delta = clock.ElapsedMilliseconds - now; // Get time at top of loop

                // Check if there is a valid delta target, and if the loop is ahead of the target
                if (TargetDelta > 0 && Delta < TargetDelta)
                {
                    continue; // Waste time until on schedule
                }
                
                now = clock.ElapsedMilliseconds; // Set current time
                
                Tick?.Invoke(Delta);
            }
            Shutdown?.Invoke();
        }
    }
}