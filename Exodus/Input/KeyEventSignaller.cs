using System;
using System.Collections.Generic;

namespace Exodus.Input
{
    /// <summary>
    /// Handle registering key event listeners and invoking them when their event has occured.
    /// </summary>
    public class KeyEventSignaller
    {
        /// <summary>
        /// A list of registered listener methods.
        /// </summary>
        private Dictionary<Key, Action<KeyState>> _listeners = new();

        /// <summary>
        /// Register event polling to the engine game tick loop.
        /// </summary>
        internal KeyEventSignaller()
        {
            ExodusEngine.Instance.Tick += PollKeyEvents;
        }
        
        /// <summary>
        /// Register a method to be called when its key event has occured.
        /// </summary>
        /// <param name="key">Key to listen for</param>
        /// <param name="action">Method to invoke on event</param>
        public void RegisterListener(Key key, Action<KeyState> action)
        {
            _listeners.Add(key, action);
        }

        /// <summary>
        /// Poll all registered keys for events during current game tick, and invoke the listener method if an event
        /// has occured.
        /// </summary>
        /// <param name="delta">Time since last game tick</param>
        private void PollKeyEvents(long delta)
        {
            foreach (Key k in _listeners.Keys)
            {
                var e = ExodusEngine.Instance.KeyboardInput.PollKeyEvent(k);
                if (e is { } val)
                {
                    _listeners[k].Invoke(val);
                }
            }
            
        }
    }
}