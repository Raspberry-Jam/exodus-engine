using OpenTK.Windowing.GraphicsLibraryFramework;

namespace Exodus.Input
{
    /// <summary>
    /// Handles keyboard input polling and events.
    /// </summary>
    public class KeyboardInput
    {
        /// <summary>
        /// Restrict default constructor to internal use only.
        /// </summary>
        internal KeyboardInput()
        {
        }

        /// <summary>
        /// Check if an event has occured for the specified key, and if it has, return the state of the event, otherwise
        /// return null.
        /// </summary>
        /// <param name="k">The key to check</param>
        /// <returns>State of event, or null for no event</returns>
        internal KeyState? PollKeyEvent(Key k)
        {
            var state = ExodusEngine.Instance.Window.KeyboardState;
            if (state.IsKeyPressed((Keys)k))
            {
                return KeyState.Down;
            } 
            if (state.IsKeyReleased((Keys)k))
            {
                return KeyState.Up;
            }

            return null;
        }

        /// <summary>
        /// Poll if a specific key is down during any given game tick.
        /// </summary>
        /// <param name="k">Key to poll</param>
        /// <returns></returns>
        public bool IsKeyDown(Key k)
        {
            var state = ExodusEngine.Instance.Window.KeyboardState;
            return state.IsKeyDown((Keys)k);
        }
    }
    
    /// <summary>
    /// Describes the current state (up or down) of a key on the keyboard.
    /// </summary>
    public enum KeyState
    {
        Down,
        Up
    }

    /// <summary>
    /// A list of key codes for a generic US-layout keyboard.
    /// </summary>
    public enum Key
    {
        Unknown = Keys.Unknown,
        Space = Keys.Space,
        Apostrophe = Keys.Apostrophe,
        Comma = Keys.Comma,
        Minus = Keys.Minus,
        Period = Keys.Period,
        Forwardslash = Keys.Slash,
        N0 = Keys.D0,
        N1 = Keys.D1,
        N2 = Keys.D2,
        N3 = Keys.D3,
        N4 = Keys.D4,
        N5 = Keys.D5,
        N6 = Keys.D6,
        N7 = Keys.D7,
        N8 = Keys.D8,
        N9 = Keys.D9,
        Semicolon = Keys.Semicolon,
        Equal = Keys.Equal,
        A = Keys.A,
        B = Keys.B,
        C = Keys.C,
        D = Keys.D,
        E = Keys.E,
        F = Keys.F,
        G = Keys.G,
        H = Keys.H,
        I = Keys.I,
        J = Keys.J,
        K = Keys.K,
        L = Keys.L,
        M = Keys.M,
        N = Keys.N,
        O = Keys.O,
        P = Keys.P,
        Q = Keys.Q,
        R = Keys.R,
        S = Keys.S,
        T = Keys.T,
        U = Keys.U,
        V = Keys.V,
        W = Keys.W,
        X = Keys.X,
        Y = Keys.Y,
        Z = Keys.Z,
        LeftBracket = Keys.LeftBracket,
        RightBracket = Keys.RightBracket,
        Backslash = Keys.Backslash,
        Grave = Keys.GraveAccent,
        Escape = Keys.Escape,
        Enter = Keys.Enter,
        Tab = Keys.Tab,
        Backspace = Keys.Backspace,
        Insert = Keys.Insert,
        Delete = Keys.Delete,
        Right = Keys.Right,
        Left = Keys.Left,
        Down = Keys.Down,
        Up = Keys.Up,
        PageUp = Keys.PageUp,
        PageDown = Keys.PageDown,
        Home = Keys.Home,
        End = Keys.End,
        CapsLock = Keys.CapsLock,
        ScrollLock = Keys.ScrollLock,
        NumLock = Keys.NumLock,
        PrintScreen = Keys.PrintScreen,
        Pause = Keys.Pause,
        F1 = Keys.F1,
        F2 = Keys.F2,
        F3 = Keys.F3,
        F4 = Keys.F4,
        F5 = Keys.F5,
        F6 = Keys.F6,
        F7 = Keys.F7,
        F8 = Keys.F8,
        F9 = Keys.F9,
        F10 = Keys.F10,
        F11 = Keys.F11,
        F12 = Keys.F12,
        F13 = Keys.F13,
        F14 = Keys.F14,
        F15 = Keys.F15,
        F16 = Keys.F16,
        F17 = Keys.F17,
        F18 = Keys.F18,
        F19 = Keys.F19,
        F20 = Keys.F20,
        F21 = Keys.F21,
        F22 = Keys.F22,
        F23 = Keys.F23,
        F24 = Keys.F24,
        F25 = Keys.F25,
        KeyPad0 = Keys.KeyPad0,
        KeyPad1 = Keys.KeyPad1,
        KeyPad2 = Keys.KeyPad2,
        KeyPad3 = Keys.KeyPad3,
        KeyPad4 = Keys.KeyPad4,
        KeyPad5 = Keys.KeyPad5,
        KeyPad6 = Keys.KeyPad6,
        KeyPad7 = Keys.KeyPad7,
        KeyPad8 = Keys.KeyPad8,
        KeyPad9 = Keys.KeyPad9,
        KeyPadDecimal = Keys.KeyPadDecimal,
        KeyPadDivide = Keys.KeyPadDivide,
        KeyPadMultiply = Keys.KeyPadMultiply,
        KeyPadSubtract = Keys.KeyPadSubtract,
        KeyPadAdd = Keys.KeyPadAdd,
        KeyPadEnter = Keys.KeyPadEnter,
        KeyPadEqual = Keys.KeyPadEqual,
        LeftShift = Keys.LeftShift,
        LeftControl = Keys.LeftControl,
        LeftAlt = Keys.LeftAlt,
        LeftSuper = Keys.LeftSuper,
        RightShift = Keys.RightShift,
        RightControl = Keys.RightControl,
        RightAlt = Keys.RightAlt,
        RightSuper = Keys.RightSuper,
        LastKey = Keys.LastKey,
        Menu = Keys.Menu,
    }
}