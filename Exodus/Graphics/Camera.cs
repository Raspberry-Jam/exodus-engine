using OpenTK.Mathematics;

namespace Exodus.Graphics
{
    public class Camera
    {
        /// <summary>
        /// Camera projection matrix.
        /// </summary>
        public Matrix4 Projection { get; private set; }
        
        /// <summary>
        /// Camera view transformation matrix.
        /// </summary>
        public Matrix4 View { get; private set; }

        /// <summary>
        /// Camera position coordinates in world space.
        /// </summary>
        public Vector2 Position
        {
            get => _position;
            set
            {
                _position = value;
                RecalculateViewMatrix();
            }
        }
        private Vector2 _position = Vector2.Zero;

        /// <summary>
        /// Camera rotation around Z-axis.
        /// </summary>
        public float Rotation
        {
            get => _rotation;
            set
            {
                _rotation = value;
                RecalculateViewMatrix();
            }
        }
        private float _rotation = 0.0f;

        /// <summary>
        /// World geometry scale (camera zoom).
        /// </summary>
        public float Scale
        {
            get => _scale;
            set
            {
                _scale = value;
                RecalculateViewMatrix();
            }
        }

        private float _scale = 1.0f;

        /// <summary>
        /// Create orthographic camera instance.
        /// </summary>
        public Camera()
        {
            var width = ExodusEngine.Instance.Window.Width;
            var height = ExodusEngine.Instance.Window.Height;
            View = Matrix4.Identity;
            Projection = Matrix4.CreateOrthographic(width, height, -1.0f, 1.0f);
            
            ExodusEngine.Instance.Window.Resize += RecalculateProjectionMatrix;
        }

        /// <summary>
        /// Recalculate projection matrix aspect ratio on window resize event.
        /// </summary>
        /// <param name="width">Width in pixels</param>
        /// <param name="height">Height in pixels</param>
        private void RecalculateProjectionMatrix(int width, int height)
        {
            Projection = Matrix4.CreateOrthographic(width, height, -1.0f, 1.0f);
        }
        
        /// <summary>
        /// Recalculate view transform matrix.
        /// </summary>
        private void RecalculateViewMatrix()
        {
            Matrix4 transform = Matrix4.CreateRotationZ(Rotation) *
                                Matrix4.CreateScale(Scale) *
                                Matrix4.CreateTranslation(new Vector3(Position));

            View = Matrix4.Invert(transform);
        }
    }
}