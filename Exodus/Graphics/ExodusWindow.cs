using System;
using OpenTK.Graphics.OpenGL;
using OpenTK.Mathematics;
using OpenTK.Windowing.Common;
using OpenTK.Windowing.Desktop;
using OpenTK.Windowing.GraphicsLibraryFramework;

namespace Exodus.Graphics
{
    internal class ExodusWindow : NativeWindow
    {
        /// <summary>
        /// The width and height of the window (excluding titlebar and other decorations) measured in pixels.
        /// </summary>
        public int Width { get; private set; }
        public int Height { get; private set; }

        public event Action<int, int> Resize;
        
        /// <summary>
        /// Create an Exodus game window.
        /// </summary>
        /// <param name="width">Window width</param>
        /// <param name="height">Window height</param>
        /// <param name="title">Window title</param>
        public ExodusWindow(int width, int height, string title) : base(
            new NativeWindowSettings
            {
                Size = new Vector2i(width, height),
                Title = title
            })
        {
            Context.MakeCurrent(); // Set the GLFW context to the current thread
            Width = width;
            Height = height;
            
            // Subscribe to ExodusEngine loop and shutdown events
            ExodusEngine.Instance.Tick += HandleEvents;
            ExodusEngine.Instance.Shutdown += Close;
        }

        /// <summary>
        /// Process the GLFW window events. Set the engine's close state to true if the window needs to close.
        /// </summary>
        private unsafe void Run()
        {
            if (GLFW.WindowShouldClose(WindowPtr))
            {
                ExodusEngine.Instance.ShouldClose = true;
                return;
            }
            Context.SwapBuffers();
            ProcessEvents();
        }

        /// <summary>
        /// Handle internal window events on game tick.
        /// </summary>
        /// <param name="delta"></param>
        private void HandleEvents(long delta)
        {
            Run();
        }

        /// <summary>
        /// Resize the OpenGL viewport at window resize events
        /// </summary>
        /// <param name="e">Resize event values</param>
        protected override void OnResize(ResizeEventArgs e)
        {
            Width = e.Width;
            Height = e.Height;
            GL.Viewport(0, 0, Width, Height);
            Resize?.Invoke(Width, Height);
            base.OnResize(e);
        }
    }
}