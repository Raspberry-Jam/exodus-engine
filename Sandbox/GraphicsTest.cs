using Exodus;
using Exodus.Debug;
using Exodus.Graphics;
using Exodus.Input;
using OpenTK.Graphics.OpenGL;
using OpenTK.Mathematics;

namespace Sandbox
{
    public class GraphicsTest
    {
        private readonly float[] _vertices =
        {
            -0.5f, -0.5f, // Bottom left
            -0.5f, 0.5f, // Top left
            0.5f, 0.5f, // Top right
            0.5f, -0.5f // Bottom right
        };

        private readonly uint[] _indices =
        {
            0, 1, 2,
            2, 3, 0
        };

        private int _vertexBufferObject;
        private int _vertexArrayObject;
        private int _elementBufferObject;
        private Shader _shader;
        private Camera _camera;

        public GraphicsTest()
        {
            ExodusEngine.Instance.Startup += Setup;
            ExodusEngine.Instance.Tick += RenderFrame;
            ExodusEngine.Instance.Tick += HandleKeys;
            ExodusEngine.Instance.Shutdown += Teardown;
        }

        private void Setup()
        {
            GL.ClearColor(0.3f, 0.3f, 0.3f, 1.0f); // Set clear colour

            _vertexArrayObject = GL.GenVertexArray();
            _vertexBufferObject = GL.GenBuffer();
            _elementBufferObject = GL.GenBuffer();

            _shader = new Shader("Shaders/shader.vert", "Shaders/shader.frag");
            _camera = new Camera();
            
            GL.BindVertexArray(_vertexArrayObject);
            GL.BindBuffer(BufferTarget.ArrayBuffer, _vertexBufferObject);
            GL.BufferData(BufferTarget.ArrayBuffer, _vertices.Length * sizeof(float), _vertices, BufferUsageHint.StaticDraw);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, _elementBufferObject);
            GL.BufferData(BufferTarget.ElementArrayBuffer, _indices.Length * sizeof(uint), _indices, BufferUsageHint.StaticDraw);
            
            GL.VertexAttribPointer(
                _shader.GetAttribLocation("aPosition"),
                2,
                VertexAttribPointerType.Float,
                false,
                2 * sizeof(float),
                0 * sizeof(float)
            );
            
            GL.EnableVertexAttribArray(_shader.GetAttribLocation("aPosition"));
            
            //_shader.Use();
            
            GL.BindVertexArray(0); // Unbind vertex array
        }

        private void RenderFrame(long delta)
        {
            using (Profiler.Method())
            {
                GL.Clear(ClearBufferMask.ColorBufferBit);
            
                _shader.Use();
                CreateClipMatrix();
            
                GL.BindVertexArray(_vertexArrayObject);
                GL.DrawElements(PrimitiveType.Triangles, _indices.Length, DrawElementsType.UnsignedInt, 0);
            }
        }

        private void HandleKeys(long delta)
        {
            const float speed = 0.5f;
            const float rot = 0.05f;
            const float scale = 2.0f;

            var step = speed * delta;
            var scaleFactor = 1.0f + scale * ((float) delta / 1000);
            var input = ExodusEngine.Instance.KeyboardInput;

            if (input.IsKeyDown(Key.W))
            {
                _camera.Position = new Vector2(_camera.Position.X, _camera.Position.Y + step);
            }

            if (input.IsKeyDown(Key.S))
            {
                _camera.Position = new Vector2(_camera.Position.X, _camera.Position.Y - step);
            }

            if (input.IsKeyDown(Key.A))
            {
                _camera.Position = new Vector2(_camera.Position.X - step, _camera.Position.Y);
            }

            if (input.IsKeyDown(Key.D))
            {
                _camera.Position = new Vector2(_camera.Position.X + step, _camera.Position.Y);
            }

            if (input.IsKeyDown(Key.R))
            {
                _camera.Scale *= scaleFactor;
            }

            if (input.IsKeyDown(Key.F))
            {
                _camera.Scale /= scaleFactor;
            }
            
        }

        private void CreateClipMatrix()
        {
            //Matrix4 model = Matrix4.Identity; // No model transformation
            Matrix4 model = Matrix4.CreateScale(100f);
            _shader.SetMat4("model", model);
            _shader.SetMat4("view", _camera.View);
            _shader.SetMat4("projection", _camera.Projection);
        }

        private void Teardown()
        {
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0); // Unbind vertex buffer
            GL.DeleteBuffer(_vertexBufferObject); // Delete vertex buffer
            _shader.Dispose();
        }
    }
}