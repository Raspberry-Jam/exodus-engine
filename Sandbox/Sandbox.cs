﻿using System;
using Exodus;
using Exodus.Debug;
using Exodus.Input;

namespace Sandbox
{
    internal class Sandbox
    {
        private static readonly NLog.Logger Log = NLog.LogManager.GetLogger("Sandbox");
        private static void Main(string[] args)
        {
            new Sandbox();
        }

        private Sandbox()
        {
            var engine = new ExodusEngine(800, 600, "This is a test");
            engine.KeyEvents.RegisterListener(Key.Escape, ExitOnEscape);
            engine.KeyEvents.RegisterListener(Key.V, ActivateProfiling);
            engine.TargetDelta = 20;
            
            Profiler.ResultListener += PrintProfileResults;

            var gtest = new GraphicsTest();
            
            Log.Info("Created graphics test");
            
            engine.Run();
        }

        private void PrintProfileResults(ProfilerResult result)
        {
            Console.WriteLine($"{result.Name}: {result.Time:n2}ms");
        }

        private void ActivateProfiling(KeyState state)
        {
            if (state == KeyState.Down)
            {
                Profiler.Profiling = true;
            }
            else
            {
                Profiler.Profiling = false;
            }
        }

        private void ExitOnEscape(KeyState state)
        {
            ExodusEngine.Instance.ShouldClose = true;
        }
    }
}