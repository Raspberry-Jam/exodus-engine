#version 330 core

in vec2 aPosition;
//in vec2 aTexCoord;

//out vec2 texCoord;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main() {
     gl_Position = vec4(aPosition, 0.0, 1.0) * model * view * projection;
     //texCoord = aTexCoord;
}
