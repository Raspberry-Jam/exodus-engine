using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using OpenTK.Graphics.OpenGL;
using OpenTK.Mathematics;
using GLEnum = OpenTK.Graphics.OpenGL.All;

namespace Sandbox
{
    public class Shader : IDisposable
    {
        private readonly int _handle;

        private readonly Dictionary<string, int> _uniformLocations;

        // OpenGL shaders compile vertex and fragment shaders into a single program.
        public Shader(string vertPath, string fragPath)
        {
            var vertexShader = CreateShader(vertPath, ShaderType.VertexShader);
            var fragmentShader = CreateShader(fragPath, ShaderType.FragmentShader);

            _handle = GL.CreateProgram();
            
            GL.AttachShader(_handle, vertexShader);
            GL.AttachShader(_handle, fragmentShader);
            
            GL.LinkProgram(_handle);
            GL.GetProgram(_handle, ProgramParameter.LinkStatus, out var code);
            if (code != (int)GLEnum.True)
            {
                var infoLog = GL.GetProgramInfoLog(_handle);
                throw new ProgramLinkException($"Failed to link program {_handle}.\n{infoLog}");
            }
            
            // Program has linked shaders, now the individually compiled binaries can be dismissed.
            GL.DetachShader(_handle, vertexShader);
            GL.DetachShader(_handle, fragmentShader);
            GL.DeleteShader(vertexShader);
            GL.DeleteShader(fragmentShader);
        }

        public void Use()
        {
            GL.UseProgram(_handle);
        }

        private int CreateShader(string path, ShaderType stage)
        {
            int shader = GL.CreateShader(stage);
            string source;

            using (StreamReader reader = new StreamReader(path, Encoding.UTF8))
            {
                source = reader.ReadToEnd();
            }

            GL.ShaderSource(shader, source);
            GL.CompileShader(shader);
            GL.GetShader(shader, ShaderParameter.CompileStatus, out var code);
            if (code != (int)GLEnum.True)
            {
                var infoLog = GL.GetShaderInfoLog(shader);
                throw new ShaderCompileException($"Failed to compile shader {shader} at path {path}.\n{infoLog}");
            }
            
            return shader;
        }

        private bool _disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (_disposedValue) return;
            GL.DeleteProgram(_handle);
            _disposedValue = true;
        }

        ~Shader()
        {
            GL.DeleteProgram(_handle);
        }
        
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public int GetAttribLocation(string attribName)
        {
            return GL.GetAttribLocation(_handle, attribName);
        }

        public void SetInt(string name, int value)
        {
            int location = GL.GetUniformLocation(_handle, name);
            GL.Uniform1(location, value);
        }

        public void SetMat4(string name, Matrix4 value)
        {
            int location = GL.GetUniformLocation(_handle, name);
            // Second argument specifies if matrix layout should be transposed.
            // OpenTK uses row-major, whereas GLSL expects column-major, so this should almost always be true.
            GL.UniformMatrix4(location, true, ref value);
        }

        public void SetVec4(string name, ref Vector4 value)
        {
            int location = GL.GetUniformLocation(_handle, name);
            GL.Uniform4(location, value);
        }

        public int GetUniformLocation(string uniformName)
        {
            return GL.GetUniformLocation(_handle, uniformName);
        }
    }

    public class ShaderCompileException : Exception
    {
        public ShaderCompileException()
        {
        }

        public ShaderCompileException(string message) : base(message)
        {
        }
    }

    public class ProgramLinkException : Exception
    {
        public ProgramLinkException()
        {
        }
        public ProgramLinkException(string message) : base(message)
        {
        }
    }
}