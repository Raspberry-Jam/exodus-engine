# Exodus Engine
Exodus Engine is an experiment written in C#, to build a game engine using OpenGL. The intention is to construct a basic game engine capable of complex 2D graphics, basic Axis-Aligned Bounding Box collision and physics, and handling potentially hundreds of entities simultaneously.

Currently it has a keyboard input system, a debug performance profiler, and a basic abstraction of a rendering API built over OpenGL. The rendering API still requires a centralised texture loading system.

### Structure
This repo contains 2 projects; the Exodus library project and the Sandbox executable project. Exodus is where stable/complete implementations that have appropriate documentation go, and Sandbox is used to both test the Exodus library, and as a playground to experiment with the structure of new systems, which may eventually be implemented into Exodus.
